# init
init subject form 42 school
# PDF
- [link to pdf](https://cdn.intra.42.fr/pdf/pdf/750/init.fr.pdf)
***
# color code
- ![#ff0000](https://placehold.it/15/ff0000/000000?text=+) `description`
- ![#0000ff](https://placehold.it/15/0000ff/000000?text=+) `command`
- ![#00ff00](https://placehold.it/15/00ff00/000000?text=+) `command output`

***
